package cql.ecci.ucr.ac.lab6;

import android.content.Context;

import java.util.List;

// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes
public interface ItemsRepository {

    List<Persona> obtainItems(Context context) throws CantRetrieveItemsException;
}