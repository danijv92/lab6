package cql.ecci.ucr.ac.lab6;

import java.util.List;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public interface DataBaseDataSource {
    List<Persona> obtainItems () throws BaseDataItemsException;
}

