package cql.ecci.ucr.ac.lab6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


public class DetailsActivity extends AppCompatActivity implements DetailsActivityView {

    public static final int fragmentContainerId = R.id.details_fragments_container;

    private DetailsActivityPresenterImpl detailsActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //Llama al presenter de esta actividad
        detailsActivityPresenter = new DetailsActivityPresenterImpl(this, getApplicationContext());

        if (savedInstanceState == null) {

            //Mete el fragmento de texto en el contenedor, pues es el fragmento "principal"
            //El fragmento mapa debe remplazarlo cuando sea necesario
            getSupportFragmentManager().beginTransaction()
                    .add(fragmentContainerId, DetailsFragment.newInstance())
                    .commitNow();
        }
    }

    @Override
    protected void onResume() {
        detailsActivityPresenter.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        detailsActivityPresenter.onDestroy();
        super.onDestroy();
    }

}
