package cql.ecci.ucr.ac.lab6;

public class BaseDataItemsException extends Exception {

    public BaseDataItemsException(String msg) {
        super(msg);
    }
}