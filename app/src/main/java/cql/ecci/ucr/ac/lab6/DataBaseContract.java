package cql.ecci.ucr.ac.lab6;

import android.provider.BaseColumns;

public final class DataBaseContract {

    private DataBaseContract() { }

        // Definimos una clase interna que define las tablas y columnas
        // Implementa la interfaz BaseColumns para heredar campos estandar del marco de Android _ID
        public static class DataBaseEntry implements BaseColumns {

            // Clase Persona
            public static final String TABLE_NAME_PERSONA = "Persona";

            // private String identificacion
            public static final String COLUMN_NAME_IDENTIFICACION = "identificacion";

            // private String Nombre
            public static final String COLUMN_NAME_NOMBRE = "nombre";

            // private String carnet;
            public static final String COLUMN_NAME_CARNET = "carnet";
        }


        // Construir las tablas de la base de datos
        private static final String TEXT_TYPE = " TEXT";
        private static final String COMMA_SEP = ",";


        // Creacion de tabla Persona
        public static final String SQL_CREATE_PERSONA =
                "CREATE TABLE " + DataBaseEntry.TABLE_NAME_PERSONA + " (" +
                        DataBaseEntry.COLUMN_NAME_IDENTIFICACION + TEXT_TYPE + "PRIMARY KEY," +
                        DataBaseEntry.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
                        DataBaseEntry.COLUMN_NAME_CARNET + TEXT_TYPE +  " )";

        public static final String SQL_DELETE_PERSONA =
                "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_PERSONA;

}

