package cql.ecci.ucr.ac.lab6;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class PersonaImpl implements ModeloPersona {

    public long insertar(Context context, Persona persona)
    {

        // usar la clase DataBaseHelper para realizar la operacion de insertar
        DataBaseHelperImpl dataBaseHelperImpl = new DataBaseHelperImpl(context);

        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelperImpl.getWritableDatabase();



        ContentValues values = new ContentValues();

        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_IDENTIFICACION, persona.getId());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE, persona.getNombre());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CARNET, persona.getCarnet());

        return db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA, null, values);
    }

}
