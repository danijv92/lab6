package cql.ecci.ucr.ac.lab6;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class DetailsFragment extends Fragment implements DetailsFragmentView {

    private View v;
    private Persona persona;
    private DetailsFragmentPresenter fragmentPresenter;


    public DetailsFragment() {

    }

    public static DetailsFragment newInstance() {
        DetailsFragment fragment = new DetailsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_details, container, false);
        Bundle bundle = getActivity().getIntent().getExtras();

        //Crea el presenter del fragmento
        fragmentPresenter = new DetailsFragmentPresenterImpl(this, getContext());

        if(bundle != null)
        {
            persona = bundle.getParcelable("PERSONA");

            //Instancia todos los campos del layout
            TextView nombre = (TextView) v.findViewById(R.id.nombre);
            TextView id = (TextView) v.findViewById(R.id.id);
            TextView carnet = (TextView) v.findViewById(R.id.carnet);
            ImageView ImageView = (ImageView) v.findViewById(R.id.imagen);

            int imageId = R.drawable.profile;

            //Despliega la información del tabletop en los campos respectivos
            nombre.setText(persona.getNombre());
            id.setText("ID: " + persona.getId());
            carnet.setText("Carnet: " + persona.getCarnet());
            ImageView.setImageResource(imageId);
        }

        return v;
    }

    @Override
    public void onResume()
    {
        fragmentPresenter.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy()
    {
        fragmentPresenter.onDestroy();
        super.onDestroy();
    }
}
