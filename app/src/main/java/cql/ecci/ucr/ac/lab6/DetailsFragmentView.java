package cql.ecci.ucr.ac.lab6;

public interface DetailsFragmentView
{
    public void onResume();

    public void onDestroy();
}
