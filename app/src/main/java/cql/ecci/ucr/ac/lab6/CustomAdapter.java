package cql.ecci.ucr.ac.lab6;

public interface CustomAdapter
{
    Object getItem(int position);
}