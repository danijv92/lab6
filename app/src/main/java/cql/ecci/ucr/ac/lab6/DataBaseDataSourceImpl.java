package cql.ecci.ucr.ac.lab6;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public class DataBaseDataSourceImpl implements DataBaseDataSource {
    private List<Persona> items;
    private Context context;

    public DataBaseDataSourceImpl(Context context) {

        this.context = context;
    }

    @Override
    public List<Persona> obtainItems() throws BaseDataItemsException {

        items = new ArrayList<Persona>();
        try {
            getPersonas();
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
        return items;
    }

    // Esta lista debe recuperarla de la base de datos
    // para el ejemplo la inicializamos con datos dummy
    private void getPersonas() {
        DataBaseHelperImpl dataBaseHelperImpl = new DataBaseHelperImpl(context);
        SQLiteDatabase db = dataBaseHelperImpl.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from PERSONA",null);

        if (cursor.moveToFirst()) {

            while (!cursor.isAfterLast()) {

                this.items.add(new Persona(cursor.getString(0),
                                            cursor.getString(1),
                                            cursor.getString(2)));
                        cursor.moveToNext();
            }
        }
    }
}