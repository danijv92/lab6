package cql.ecci.ucr.ac.lab6;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomAdapterImpl extends BaseAdapter implements CustomAdapter {

    private Context context;
    private List<Persona> list;


    public CustomAdapterImpl(Context context, List<Persona> list)
    {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount()
    {
        return list.size();
    }

    @Override

    public Object getItem(int position)
    {
        return list.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.list, parent, false);
        Persona actualPersona = list.get(position);

        // Se instancian los campos de la vista
        TextView nameText = (TextView) rowView.findViewById(R.id.name);
        TextView carnetText = (TextView)rowView.findViewById(R.id.carnet);
        ImageView imageView = (ImageView)rowView.findViewById(R.id.image);

        // Se setean los campos de la vista
        nameText.setText(actualPersona.getNombre());
        carnetText.setText(actualPersona.getCarnet());

        int image = R.drawable.profile;
        imageView.setImageResource(image);

        return rowView;
    }

}
