package cql.ecci.ucr.ac.lab6;

public interface DetailsActivityPresenter
{
    void onResume();

    void onDestroy();
}