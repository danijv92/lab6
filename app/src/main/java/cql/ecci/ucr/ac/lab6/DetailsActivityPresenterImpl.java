package cql.ecci.ucr.ac.lab6;

import android.content.Context;

public class DetailsActivityPresenterImpl implements DetailsActivityPresenter {
    DetailsActivityView detailsActivity;
    Context context;

    DetailsActivityPresenterImpl(DetailsActivityView detailsActivityView, Context context) {
        this.detailsActivity = detailsActivityView;
    }

    @Override
    public void onResume() {}

    @Override
    public void onDestroy() {

        this.detailsActivity = null;
    }

}