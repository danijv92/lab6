package cql.ecci.ucr.ac.lab6;

public class CantRetrieveItemsException extends Exception {

    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}