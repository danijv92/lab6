package cql.ecci.ucr.ac.lab6;

import android.content.Context;

public class DetailsFragmentPresenterImpl implements DetailsFragmentPresenter
{
    private DetailsFragmentView detailsFragment;
    private Context context;

    DetailsFragmentPresenterImpl(DetailsFragmentView detailsFragmentView, Context context)
    {
        this.detailsFragment = detailsFragmentView;
        this.context = context;
    }

    @Override
    public void onResume()
    {

    }

    @Override
    public void onDestroy()
    {
        detailsFragment = null;
    }
}