package cql.ecci.ucr.ac.lab6;

public interface DetailsFragmentPresenter
{
    public void onResume();

    public void onDestroy();
}
